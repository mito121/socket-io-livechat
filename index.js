const express = require("express");
const app = express();
const http = require("http");
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server);
const PORT = process.env.PORT || 3000;

let activeUsers = [],
  typingUsers = [];

/* Serve index.html */
app.get("/", (req, res) => {
  res.sendFile(__dirname + "/index.html");
});

/* Serve stylesheet */
app.get("/style.css", (req, res) => {
  res.sendFile(__dirname + "/style.css");
});

/* On user connect */
io.on("connection", (socket) => {
  /* Listen for new-user emit from client, to broadcast when a user joins the chat */
  socket.on("new-user", (user) => {
    activeUsers.push({ socketId: socket.id, user: user });
    socket.emit("update-users", activeUsers);
    socket.broadcast.emit("connected", user);

    io.to(socket.id).emit("set-socket", socket.id);
  });

  /* Listen for is-typing emit */
  socket.on("typing", ({ username, typing } = obj) => {
    // if (typing == false)
    //   return socket.broadcast.emit("typing", { user: user, typing: false });
    // socket.broadcast.emit("typing", { user: user, typing: true });

    /* Add user to typing array, if not already there, and typing is true */
    // if (!typingUsers.includes(user) && typing)
    if (!typingUsers.some((usr) => usr.socketId == socket.id) && typing)
      typingUsers.push({ socketId: socket.id, user: username });

    /* Remove user from typing array if there, and typing is false */
    if (typingUsers.some((usr) => usr.socketId == socket.id) && !typing) {
      let typingUsersUpdated = typingUsers.filter(
        (usr) => usr.socketId !== socket.id
      );
      typingUsers = [...typingUsersUpdated];
      return socket.broadcast.emit("typing", typingUsers);
    }

    socket.broadcast.emit("typing", typingUsers);
  });

  /* Listen for chat-message emit from client, then broadcast message to all other users */
  socket.on("chat-message", (user, message) => {
    socket.broadcast.emit("chat-message", user, message); // broadcast rather than emit to display <You> rather than <Your Name> when sending a message
  });

  /* Emit updates to activeUsers for all connected sockets */
  socket.on("update-users", () => {
    socket.emit("active-users", activeUsers);
  });

  /* On user disconnect */
  socket.on("disconnect", () => {
    /* Get user name of disconnected user */
    const user = activeUsers.filter((user) => user.socketId == socket.id);

    /* If user disconnected while typing, also remove user from typing array */
    if (typingUsers.some((usr) => usr.socketId == socket.id)) {
      let typingUsersUpdated = typingUsers.filter(
        (usr) => usr.socketId !== socket.id
      );
      typingUsers = [...typingUsersUpdated];
      socket.broadcast.emit("typing", typingUsers);
    }

    /* Remove disconnected user from activeUsers array */
    const updatedActiveUsers = activeUsers.filter(function (usr) {
      return usr.socketId !== socket.id;
    });

    /* Update activeUsers array */
    activeUsers = [...updatedActiveUsers];

    /* Broadcast disconnection */
    socket.broadcast.emit("disconnected", user);
  });
});

server.listen(PORT, () => {
  console.log(`listening on *:${PORT}`);
});
