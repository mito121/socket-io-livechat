# Real-time chat using socket.io
To run this application, clone repo and run following commands:

```
1. npm i

2. npm run chat
```

To see this app live in action visit: [Example chat app](https://example-chat-app.herokuapp.com/)
